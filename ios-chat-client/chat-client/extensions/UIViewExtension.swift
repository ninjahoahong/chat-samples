import UIKit

enum ConstraintType {
    case less
    case equal
    case greater
}

struct ConstraintTypes {
    let top: ConstraintType
    let left: ConstraintType
    let bottom: ConstraintType
    let right: ConstraintType
    init(top: ConstraintType, left: ConstraintType, bottom: ConstraintType, right: ConstraintType) {
        self.top = top
        self.left = left
        self.right = right
        self.bottom = bottom
    }
}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor? = nil, leading: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, trailing: NSLayoutXAxisAnchor? = nil, constraintTypes: ConstraintTypes = ConstraintTypes(top: .equal, left: .equal, bottom: .equal, right: .equal), padding: UIEdgeInsets = .zero, size: CGRect = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            switch constraintTypes.top {
            case .greater:
                NSLayoutConstraint.activate([topAnchor.constraint(greaterThanOrEqualTo: top, constant: padding.top)])
            case .less:
                NSLayoutConstraint.activate([topAnchor.constraint(lessThanOrEqualTo: top, constant: padding.top)])
            default:
                NSLayoutConstraint.activate([topAnchor.constraint(equalTo: top, constant: padding.top)])
            }
        }
        if let leading = leading {
            switch constraintTypes.left {
            case .greater:
                NSLayoutConstraint.activate([leadingAnchor.constraint(greaterThanOrEqualTo: leading, constant: padding.left)])
            case .less:
                NSLayoutConstraint.activate([leadingAnchor.constraint(lessThanOrEqualTo: leading, constant: padding.left)])
            default:
                NSLayoutConstraint.activate([leadingAnchor.constraint(equalTo: leading, constant: padding.left)])
            }
        }
        if let bottom = bottom {
            switch constraintTypes.bottom {
            case .greater:
                NSLayoutConstraint.activate([bottomAnchor.constraint(greaterThanOrEqualTo: bottom, constant: -padding.bottom)])
            case .less:
                NSLayoutConstraint.activate([bottomAnchor.constraint(lessThanOrEqualTo: bottom, constant: -padding.bottom)])
            default:
                NSLayoutConstraint.activate([bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom)])
            }
        }
        if let trailing = trailing {
            switch constraintTypes.right {
            case .greater:
                NSLayoutConstraint.activate([trailingAnchor.constraint(greaterThanOrEqualTo: trailing, constant: -padding.right)])
            case .less:
                NSLayoutConstraint.activate([trailingAnchor.constraint(lessThanOrEqualTo: trailing, constant: -padding.right)])
            default:
                NSLayoutConstraint.activate([trailingAnchor.constraint(equalTo: trailing, constant: -padding.right)])
            }
        }
        if (size.width != 0) {
            NSLayoutConstraint.activate([widthAnchor.constraint(equalToConstant: size.width)])
        }
        if (size.height != 0) {
            NSLayoutConstraint.activate([heightAnchor.constraint(equalToConstant: size.height)])
        }
    }
}

