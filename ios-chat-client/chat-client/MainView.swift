import UIKit
import Foundation
import os

class MainView: UIView {
    let TAG = "MainView"
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }
    
    func initView() {
        os_log("%s: initView", type: .debug, TAG)
        self.backgroundColor = .green
    }
}

#if DEBUG 
import SwiftUI
@available(iOS 13.0, *)
struct MainViewRepresentable: UIViewRepresentable {
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<MainViewRepresentable>) {
        
    }
    
    func makeUIView(context: Context) -> UIView {
        return MainView()
    }
}

@available(iOS 13.0, *)
struct MainView_Preview: PreviewProvider {
    static var previews: some View {
        MainViewRepresentable()
    }
}

@available(iOS 13.0, *)
struct MainViewLandScape_Preview: PreviewProvider {
    static var previews: some View {
        MainViewRepresentable()
            .previewDisplayName("iPhone 7 LandScape")
            .previewDevice(PreviewDevice(rawValue: "iPhone 7"))
            .previewLayout(.fixed(width: 667, height: 375))
    }
}
#endif
