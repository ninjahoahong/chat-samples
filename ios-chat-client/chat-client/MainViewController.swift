import UIKit
import Foundation
import os

class MainViewController: UIViewController {
    
    let mainView: UIView = {
        let uiView = MainView()
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mainView)
        view.backgroundColor = .red
        mainView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        mainView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 32, left: 0, bottom: 0, right: 0))
    }
}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor, leading: NSLayoutXAxisAnchor, bottom: NSLayoutYAxisAnchor, trailing: NSLayoutXAxisAnchor, padding: UIEdgeInsets = .zero) {
        
        topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
    }
}

#if DEBUG
import SwiftUI
@available(iOS 13.0, *)
struct MainViewControllerRepresentable: UIViewControllerRepresentable {
    func makeUIViewController(context: UIViewControllerRepresentableContext<MainViewControllerRepresentable>) -> MainViewController {
        return MainViewController()
    }
    
    func updateUIViewController(_ uiViewController: MainViewController, context: UIViewControllerRepresentableContext<MainViewControllerRepresentable>) {
    }
    
}

@available(iOS 13.0, *)
struct MainViewController_Preview: PreviewProvider {
    static var previews: some View {
        Group {
            MainViewControllerRepresentable()
                .previewDisplayName("iPhone 7")
                .previewDevice(PreviewDevice(rawValue: "iPhone 8"))
            
            MainViewControllerRepresentable()
                .previewDisplayName("iPhone 7 LandScape")
                .previewDevice(PreviewDevice(rawValue: "iPhone 8"))
                .previewLayout(.fixed(width: 667, height: 375))
        }
        
    }
}
#endif
