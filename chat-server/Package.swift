// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "chat-server",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "chat-server",
            targets: ["chat-server"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/IBM-Swift/Kitura.git", .exact("2.9.1")),
        .package(url: "https://github.com/IBM-Swift/HeliumLogger.git", .exact("1.9.0")),
        .package(url: "https://github.com/IBM-Swift/Kitura-WebSocket", .exact("2.1.2")),
        .package(url: "https://github.com/IBM-Swift/LoggerAPI.git", .exact("1.9.0"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "chat-server",
            dependencies: ["Kitura", "HeliumLogger", "Kitura-WebSocket", "LoggerAPI"],
            path: "Sources/ChatServer")
    ]
)
